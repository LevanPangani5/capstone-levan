/* eslint-disable no-unused-vars */
import { DataTypes, Model, Optional, Sequelize } from 'sequelize';
import { Models } from '../interfaces/general';

interface FeedbackAttributes {
  id: number;
  fromUser: number;
  toUser: number;
  context: string;
  companyName: string;
}

export const feedbackFormat: string[] = [
  'id',
  'fromUser',
  'companyName',
  'toUser',
  'context',
];

export class Feedback
  extends Model<FeedbackAttributes, Optional<FeedbackAttributes, 'id'>>
  implements FeedbackAttributes
{
  id: number;

  fromUser: number;

  toUser: number;

  context: string;

  companyName: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize): void {
    Feedback.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        fromUser: {
          field: 'from_user',
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: false,
          allowNull: false,
          references: {
            model: 'User',
            key: 'id',
          },
        },
        toUser: {
          field: 'to_user',
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: false,
          allowNull: false,
          references: {
            model: 'User',
            key: 'id',
          },
        },
        context: {
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        companyName: {
          field: 'company_name',
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
      },
      {
        tableName: 'feedbacks',
        underscored: true,
        sequelize,
      },
    );
  }

  static async paginateFeedbacks(
    pageSize: number,
    page: number,
  ): Promise<FeedbackAttributes[]> {
    try {
      if (pageSize <= 0 || page <= 0) {
        throw new Error('Invalid pageSize or page value');
      }

      const feedbacks = await Feedback.findAll({
        limit: pageSize,
        offset: (page - 1) * pageSize,
        attributes: feedbackFormat,
      });

      return feedbacks;
    } catch (error) {
      throw new Error(`Error while paginating Feedbacks: ${error.message}`);
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  static associate(models: Models, sequelize: Sequelize): void {
    // Example of how to define a association.
    Feedback.belongsTo(models.user, {
      foreignKey: 'from_user',
      as: 'fromUserId',
    });
    Feedback.belongsTo(models.user, { foreignKey: 'to_user', as: 'toUserId' });
  }
}
