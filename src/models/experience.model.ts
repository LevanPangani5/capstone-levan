/* eslint-disable no-unused-vars */
import { DataTypes, Model, Optional, Sequelize } from 'sequelize';
import { Models } from '../interfaces/general';

interface ExperienceAttributes {
  id: number;
  userId: number;
  companyName: string;
  role: string;
  description: string;
  startDate: Date;
  endDate: Date;
}

export const experienceFormat: string[] = [
  'id',
  'userId',
  'companyName',
  'role',
  'startDate',
  'endDate',
  'description',
];

export class Experience
  extends Model<ExperienceAttributes, Optional<ExperienceAttributes, 'id'>>
  implements ExperienceAttributes
{
  id: number;

  userId: number;

  companyName: string;

  role: string;

  description: string;

  startDate: Date;

  endDate: Date;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize): void {
    Experience.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
          field: 'user_id',
          type: DataTypes.INTEGER.UNSIGNED,
          allowNull: false,
          references: {
            model: 'User',
            key: 'id',
          },
        },
        companyName: {
          field: 'company_name',
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        role: {
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        startDate: {
          field: 'start_date',
          type: new DataTypes.DATE(),
          allowNull: false,
        },
        endDate: {
          field: 'end_date',
          type: new DataTypes.DATE(),
        },
        description: {
          type: new DataTypes.STRING(256),
        },
      },
      {
        tableName: 'experiences',
        underscored: true,
        sequelize,
      },
    );
  }

  static async paginateExperiences(
    pageSize: number,
    page: number,
  ): Promise<ExperienceAttributes[]> {
    try {
      if (pageSize <= 0 || page <= 0) {
        throw new Error('Invalid pageSize or page value');
      }
      const experiences = await this.findAll({
        limit: pageSize,
        offset: (page - 1) * pageSize,
        attributes: experienceFormat,
      });

      return experiences;
    } catch (error) {
      throw new Error(`Error while paginating Experiences: ${error.message}`);
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  static associate(models: Models, sequelize: Sequelize): void {
    // Example of how to define a association.
    Experience.belongsTo(models.user, { foreignKey: 'user_id' });
  }
}
