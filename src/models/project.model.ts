/* eslint-disable no-unused-vars */
import { DataTypes, Model, Optional, Sequelize } from 'sequelize';
import { Models } from '../interfaces/general';

interface ProjectAttributes {
  id: number;
  userId: number;
  image: string;
  description: string;
}

export const projectFormat = ['id', 'userId', 'image', 'description'];

export class Project
  extends Model<ProjectAttributes, Optional<ProjectAttributes, 'id'>>
  implements ProjectAttributes
{
  id: number;

  userId: number;

  image: string;

  description: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  static defineSchema(sequelize: Sequelize): void {
    Project.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        userId: {
          field: 'user_id',
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: false,
          allowNull: false,
        },
        image: {
          field: 'image',
          type: new DataTypes.STRING(256),
          allowNull: false,
        },

        description: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
      },
      {
        tableName: 'projects',
        underscored: true,
        sequelize,
      },
    );
  }

  static async paginateProjects(
    pageSize: number,
    page: number,
  ): Promise<ProjectAttributes[]> {
    try {
      if (pageSize <= 0 || page <= 0) {
        throw new Error('Invalid pageSize or page value');
      }
      const projects = await this.findAll({
        limit: pageSize,
        offset: (page - 1) * pageSize,
        attributes: ['id', 'userId', 'image', 'description'],
      });

      return projects;
    } catch (error) {
      throw new Error(`Error while paginating Projects: ${error.message}`);
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  static associate(models: Models, sequelize: Sequelize): void {
    // Example of how to define a association.
    Project.belongsTo(models.user, {
      foreignKey: 'user_id',
    });
  }
}
