/* eslint-disable @typescript-eslint/no-unused-vars */
import { DataTypes, Model, Optional, Sequelize } from 'sequelize';
import { Models } from '../interfaces/general';
import bcrypt from 'bcrypt';
import 'dotenv/config';

export enum UserRole {
  Admin = 'Admin',
  User = 'User',
}

export const userFormat: string[] = [
  'id',
  'firstName',
  'lastName',
  'title',
  'summary',
  'email',
  'role',
];

interface UserAttributes {
  id: number;
  firstName: string;
  lastName: string;
  image: string;
  title: string;
  summary: string;
  role: UserRole;
  email: string;
  password: string;
}

//type FormattedUser= Exclude<UserAttributes,'image'|'password'>;
type FormattedUser = Omit<Omit<UserAttributes, 'image'>, 'password'>;
export class User
  extends Model<UserAttributes, Optional<UserAttributes, 'id'>>
  implements UserAttributes
{
  id: number;

  firstName: string;

  lastName: string;

  image: string;

  title: string;

  summary: string;

  role: UserRole;

  email: string;

  password: string;

  readonly createdAt: Date;

  readonly updatedAt: Date;

  public async comparePassword(candidatePassword: string): Promise<boolean> {
    return bcrypt.compare(candidatePassword, this.password);
  }

  static defineSchema(sequelize: Sequelize): void {
    User.init(
      {
        id: {
          type: DataTypes.INTEGER.UNSIGNED,
          autoIncrement: true,
          primaryKey: true,
        },
        firstName: {
          field: 'first_name',
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        lastName: {
          field: 'last_name',
          type: new DataTypes.STRING(128),
          allowNull: false,
        },
        image: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        title: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        summary: {
          type: new DataTypes.STRING(256),
          allowNull: false,
        },
        role: {
          type: new DataTypes.STRING(50),
          allowNull: false,
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false,
          unique: true,
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false,
        },
      },

      {
        tableName: 'users',
        underscored: true,
        sequelize,
        hooks: {
          async beforeSave(user, options) {
            if (user.changed('password')) {
              try {
                const hash = await bcrypt.hash(
                  user.password,
                  +process.env.BCRYPT_SALT,
                );
                user.password = hash;
              } catch (err) {
                throw new Error('Error during hashing the password');
              }
            }
          },
        },
      },
    );
  }

  static async paginateUsers(
    pageSize: number,
    page: number,
  ): Promise<FormattedUser[]> {
    try {
      if (pageSize <= 0 || page <= 0) {
        throw new Error('Invalid pageSize or page value');
      }

      const users = await this.findAll({
        limit: pageSize,
        offset: (page - 1) * pageSize,
        attributes: userFormat,
      });

      return users;
    } catch (error) {
      throw new Error(`Error while paginating users: ${error.message}`);
    }
  }

  static associate(models: Models, sequelize: Sequelize): void {
    // Example of how to define a association.
    User.hasMany(models.project, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
    });
    User.hasMany(models.experience, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
    });

    User.hasMany(models.feedback, {
      foreignKey: 'from_user',
      as: 'feedbackFrom',
      onDelete: 'CASCADE',
    });
    User.hasMany(models.feedback, {
      foreignKey: 'to_user',
      as: 'feedbackTo',
      onDelete: 'CASCADE',
    });
  }
}
