import { logger } from './libs/logger';
import { loadApp } from './loaders/app';

(async () => {
  try{
    const app = await loadApp();

    app.listen(3001, () =>
      console.log(`Application is running on http://localhost:3001`),
    );
  }catch(err){
    logger.error(err);
  }
})();
