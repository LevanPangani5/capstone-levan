/* eslint-disable no-unused-vars */
import express from 'express';
import { AuthService } from '../services/auth.service';
import { User } from '../models/user.model';
import { Experience } from '../models/experience.model';
import { Project } from '../models/project.model';
import { Feedback } from '../models/feedback.model';
import { ExperienceService } from '../services/experience.service';
import { FeedbackService } from '../services/feedback.service';
import { ProjectService } from '../services/project.serice';
import { UserService } from '../services/user.service';
import { AuthController } from '../controllers/auth.controller';
import { ExperienceController } from '../controllers/experience.controller';
import { FeedbackController } from '../controllers/feedback.controller';
import { ProjectController } from '../controllers/project.controller';
import { UserController } from '../controllers/user.controller';

export interface Context {
  services: {
    authService: AuthService;
    expeienceService: ExperienceService;
    feedbackService: FeedbackService;
    projectService: ProjectService;
    userService: UserService;
  };
}

export interface ControllerContext{
  controllers: {
    authController: AuthController;
    expeienceController: ExperienceController;
    feedbackController: FeedbackController;
    projectController: ProjectController;
    userController: UserController;
  }
}

export type RouterFactory = (context: ControllerContext) => express.Router;

export type Loader = (app: express.Application, context: Context) => void;

export type RouterLoader = (app: express.Application, context: ControllerContext) => void;


export interface Models {
  user: typeof User;
  experience: typeof Experience;
  project: typeof Project;
  feedback: typeof Feedback;
}
