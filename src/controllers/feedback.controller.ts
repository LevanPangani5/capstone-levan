import { Request, Response } from 'express';
import { UserRole } from '../models/user.model';
import { User } from '../models/user.model';
import { statusError } from '../middleware/handleErrors';
import { FeedbackService } from '../services/feedback.service';
export class FeedbackController {
  private feedback: FeedbackService;
  constructor(feedbackservice: FeedbackService){
    this.feedback= feedbackservice;
  }
  addFeedback = async (req: Request, res: Response) => {
    const { fromUser, companyName, toUser, context } = req.body;
    const currentUser = req.user as { id: number };
  
    const toUserObj = await User.findByPk(toUser);

    if (!toUserObj) throw new statusError('toUser not found', 404);

    if (fromUser != currentUser.id) {
      throw new statusError('not allowed', 403);
    }
    const newFeedback = await this.feedback.addFeedback(
      currentUser.id,
      fromUser,
      toUser,
      context,
      companyName,     
    );
    res.status(201).json(newFeedback);
  };

  getFeedbacks = async (req: Request, res: Response) => {
    const pageSize = Number(req.query.pageSize);
    const page = Number(req.query.page);
 
    const feedbacks = await this.feedback.getFeedbacks(pageSize, page);
    res.setHeader('feedbacks-total-count', feedbacks.length);
    res.status(200).json({
      feedbacks,
    });
  };

  getFeedback = async (req: Request, res: Response) => {
    const feedbackId = Number(req.params.id);

    const feedback = await this.feedback.getFeedback(feedbackId);
   
    return res.status(200).json(feedback);
  };

  updateFeedback = async (req: Request, res: Response) => {
    const feedbackId = Number(req.params.id);
    const { fromUser, companyName, toUser, context } = req.body;
    const currentUser = req.user as { id: number; role: UserRole };
    
    const feedback = await this.feedback.updateFeedback(
      feedbackId, 
      currentUser,
      fromUser, 
      toUser, 
      context, 
      companyName,
    );
    return res.status(200).json(feedback);
  };

  deleteFeedback = async (req: Request, res: Response) => {
    const feedbackId = Number(req.params.id);
    const currentUser = req.user as { id: number; role: UserRole };
 
    await this.feedback.deleteFeedback(feedbackId, currentUser);
    return res.status(204).end();
  };
}
