import { Request, Response} from 'express';
import { UserRole } from '../models/user.model';
import {
  handleUploaded,
  uploadProjectPic,
} from '../libs/imageHelper';
import { statusError } from '../middleware/handleErrors';
import { ProjectService } from '../services/project.serice';

export class ProjectController {
  private project:ProjectService;
  constructor(projectService: ProjectService){
    this.project = projectService;
  }
  addProject = async (req: Request, res: Response) => {
    const { image, userId, description } = req.body;
    const currentUser = req.user as { id: number; role: UserRole };
    const path = image || undefined;
    if (currentUser.id != userId) {
      throw new statusError('use your user id', 403);
    }
    const imagePath = req.file
      ? handleUploaded(req.file.path)
      : await uploadProjectPic(path);
    const project = await this.project.addProject(userId, imagePath, description);
    return res.status(201).json(project);
  };

  getProjects = async (req: Request, res: Response) => {
    const pageSize = Number(req.query.pageSize);
    const page = Number(req.query.page);
    const projects = await this.project.getProjects(pageSize, page);
    res.setHeader('projects-total-count', projects.length);
    res.status(200).json({
      projects,
    });
  };

  getProject = async (req: Request, res: Response) => {
    const experienceId = Number(req.params.id); 
    const project =await this.project.getProject(experienceId);
    if (!project) throw new statusError('Project not found');

    res.status(200).json(project);
  };

  updateProject = async (req: Request, res: Response) => {
    const projectId = Number(req.params.id);
    const { userId, description } = req.body;
    const currentUser = req.user as { id: number; role: UserRole };
    const image = req.file ? handleUploaded(req.file.path) : null;
    const project = await this.project.updateProject(projectId,currentUser,userId,image,description);
    return res.status(200).json(project);
    
  };

  deleteProject = async (req: Request, res: Response) => {
    const projectId = Number(req.params.id);
    const currentUser = req.user as { id: number; role: UserRole };

    await this.project.deleteProject(projectId,currentUser);
    return res.status(204).end();
  };
}
