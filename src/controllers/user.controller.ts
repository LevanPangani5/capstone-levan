import { Request, Response} from 'express';
import {
  handleUploaded,
  uploadProfilePic,
} from '../libs/imageHelper';
import 'dotenv/config';
import { UserService } from '../services/user.service';

export class UserController {
  private user: UserService;
  constructor(userService:UserService) {
    this.user= userService;
  }

  createUser = async (req: Request, res: Response) => {
    const { firstName, lastName, title, summary, email, password, role } =
        req.body;

    const image = req.file
      ? handleUploaded(req.file.path)
      : await uploadProfilePic(role);
    const newUser = await this.user.createUser(
      firstName,
      lastName,
      email,
      password,
      role,
      title,
      summary,
      image,
    );

    return res.status(201).json(newUser);
  };

  getUsers = async (req: Request, res: Response) => {
    const pageSize = Number(req.query.pageSize);
    const page = Number(req.query.page);
    const users = await this.user.getUsers(pageSize, page);
    res.setHeader('Users-total-count', users.length);
    res.status(200).json({
      users,
    });
  };

  getUser = async (req: Request, res: Response) => {
    const userId = Number(req.params.id);
    const user = await this.user.getUser(userId);
    res.status(200).json(user);
  };

  updateUser = async (req: Request, res: Response) => {
    const userId = Number(req.params.id);
    const { firstName, lastName, title, summary, email, password, role } =req.body;
    const image = req.file ? handleUploaded(req.file.path) : null;
    const user = await this.user.updateUser(userId,firstName, lastName,email,password,role,title, summary,image);
    return res.status(200).json(user);
  };

  deleteUser = async (req: Request, res: Response) => {
    const userId = Number(req.params.id);
    await this.user.deleteUser(userId);
    req.logOut();
    return res.status(204).end();
  };

  getCV = async (req: Request, res: Response) => {
    const id = Number(req.params.userId);
    const cv = await this.user.getCV(id);
    return res.status(200).json(cv);
  };
}
