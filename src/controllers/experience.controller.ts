import { Request, Response } from 'express';
import { UserRole } from '../models/user.model';
import { statusError } from '../middleware/handleErrors';
import { ExperienceService } from '../services/experience.service';

export class ExperienceController {
  private experience : ExperienceService;
  constructor(experienceService: ExperienceService){
    this.experience = experienceService;
  }
  addExperice = async (req: Request, res: Response) => {
    const { userId, companyName, role, startDate, endDate, description } =
      req.body;
    const currentUser = req.user as { id: number };

  
    if (currentUser.id != userId) {
      throw new statusError('user with such id does not exist', 403);
    }
    const newExperience = await this.experience.addExperice(
      userId,
      companyName,
      role,
      startDate,
      endDate,
      description,
    );
    return res.status(201).json(newExperience);
  };

  getExperiences = async (req: Request, res: Response) => {
    const pageSize = Number(req.query.pageSize);
    const page = Number(req.query.page);
    
    const experiences = await this.experience.getExperiences(pageSize, page);
    res.setHeader('Experiences-total-count', experiences.length);
    res.status(200).json({
      experiences,
    });
  };

  getExperience = async (req: Request, res: Response) => {
    const experienceId = Number(req.params.id);
  
    const experience = await this.experience.getExperience(experienceId);
    res.status(200).json(experience);
  };

  updateExperience = async (req: Request,res: Response) => {
    const { userId, companyName, role, startDate, endDate, description } =
      req.body;
    const experienceId = Number(req.params.id);
    const currentUser = req.user as { id: number; role: UserRole };
  
    const experience = await this.experience.updateExperience(
      experienceId,
      currentUser,
      userId,
      companyName,
      role,
      startDate,
      endDate,
      description);
  
    res.status(200).json(experience);
  };

  deleteExperience = async (req: Request,res: Response) => {
    const experienceId = Number(req.params.id);
    const currentId = req.user as { id: number; role: UserRole };

    await this.experience.deleteExperience(experienceId,currentId);
    return res.status(204).end();
  };
}
