import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import 'dotenv/config';

import { User, UserRole } from '../models/user.model';
import { handleUploaded, uploadProfilePic } from '../libs/imageHelper';
import { AuthService } from '../services/auth.service';

export class AuthController {
  private auth:AuthService;
  constructor(authService:AuthService) {
    this.auth= authService;
  }

  register = async (req: Request, res: Response) => {
    const { firstName, lastName, title, summary, email, password } = req.body;
      
    const role = UserRole.User;
    const image:string = req.file
      ? handleUploaded(req.file.path)
      : await uploadProfilePic();
     
    const newUser= await this.auth.register(
      firstName,
      lastName,
      title,
      summary,
      email,
      password,
      role,
      image);

    return res.status(201).json(newUser);
  
  };

  login = async (req: Request, res: Response) => {
 
    const user: User = req.user as User;
    const token = jwt.sign({ userId: user.id }, process.env.JWT_SECRET, {
      expiresIn: '1h',
    });

    return res.status(200).json({
      message: 'Login successful',
      user: {
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        title: user.title,
        summary: user.summary,
        email: user.email,
        image: user.image,
      },
      token,
    });
    
  };
}
