import { Response, Request, NextFunction } from 'express';
import { UserRole } from '../models/user.model';

//checks if user can access this endpoint
const canAccess = async (req: Request, res: Response, next: NextFunction) => {
  const userId = Number(req.params.id);
  const currentUser = req.user as { id: number; role: UserRole };
  if (userId == currentUser.id || currentUser.role == UserRole.Admin) {
    next();
  } else {
    return res.status(403).json({ msg: 'Not allowed' });
  }
};

export default canAccess;
