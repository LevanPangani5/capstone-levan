import { Response, Request, NextFunction } from 'express';

const ownsAccount = async (req: Request, res: Response, next: NextFunction) => {
  const userId = Number(req.params.id);
  const currentUser = req.user as { id: number };
  if (userId !== currentUser.id) {
    return res.status(404).json({ msg: 'user with such id does not exist' });
  }
  next();
};

export default ownsAccount;
