import { Request, Response, NextFunction } from 'express';
import { ExtendedRequest } from '../interfaces/express';
import { logger } from '../libs/logger';

class statusError extends Error {
  public status;

  constructor(message?: string, status: number = 404) {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype);
    this.name = this.constructor.name;
    this.status = status;

    Error.captureStackTrace(this, this.constructor);
  }
}

function routerErrorHAndler(req: Request, res: Response, next: NextFunction) {
  const error = new statusError(
    `there is no such endpoint like: ${req.method} ${req.originalUrl}`,
  );
  return next(error);
}

function globalErrorHandler(
  error: Error,
  req: ExtendedRequest,
  res: Response,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  next: NextFunction,
){
  const statusErr = error as statusError;
  logger.error(
    `\nrequestId: ${req.id}\nstatus: ${statusErr.status || 500}\nerror: ${
      statusErr.message
    }`,
  );

  return res.status(statusErr.status || 500)
    .json({ error: statusErr.message || 'Error occured on the server ..' })
    .end();
}

export { statusError, routerErrorHAndler, globalErrorHandler };
