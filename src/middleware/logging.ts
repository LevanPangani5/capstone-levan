import { Response, NextFunction } from 'express';
import { logger } from '../libs/logger';
import { ExtendedRequest } from '../interfaces/express';

//logs basic information about request
const logging = (req: ExtendedRequest, res: Response, next: NextFunction) => {
  logger.child({ ReqId: req.id });
  logger.info(
    `\nRequest id: ${req.id}\nRequest Action: ${req.method}\nRequest rout: ${req.originalUrl}\n`,
  );
  next();
};

export { logging };
