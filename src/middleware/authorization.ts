import { UserRole } from '../models/user.model';
import { Request, Response, NextFunction } from 'express';

function roles(allowedRoles: UserRole[]) {
  return async (req: Request, res: Response, next: NextFunction) => {
    const user = req.user as { role: UserRole };

    if (user && allowedRoles.includes(user.role)) {
      next();
    } else {
      res.status(403).json({ error: 'not authorized' });
    }
  };
}

export default roles;
