import { Response, Request, NextFunction } from 'express';
import { validationResult } from 'express-validator';

//runs validations for validationg request parameters
const runValidation = async (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  next();
};

export { runValidation };
