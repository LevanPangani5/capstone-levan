import multer from 'multer';
import { Express, Request } from 'express';
import { v4 as uuidv4 } from 'uuid';

//limits size of the file
const limits = {
  fileSize: 4 * 1024 * 1024,
};

//storage setup for pictures
const storage =(path: string)=>{
  const name = path.split('/')[1];
  return multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, path);
    },
    filename: (req, file, cb) => {
      const uniqueSuffix = uuidv4();
      const ext = file.originalname.split('.').pop();
      const filename = `${file.fieldname}-${name}-${uniqueSuffix}.${ext}`;
      cb(null, filename);
    },
  });
};

//filter
const filter = (
  req: Request,
  file: Express.Multer.File,
  cb: multer.FileFilterCallback,
) => {
  if (
    file.mimetype === 'image/png' ||
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/jpeg'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};


//multer configured for projects
const projectUpload = multer({
  limits: limits,
  storage: storage('public/project/'),
  fileFilter: filter,
});

// multer configured for profiles
const profileUpload = multer({
  limits: limits,
  storage: storage('public/profile/'),
  fileFilter: filter,
});
export { projectUpload, profileUpload };
