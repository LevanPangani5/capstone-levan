import { cache } from '../loaders/redis';

// if client exists it will clear cache for given id
export const clearCache = async (id: number) => {
  if (cache !== null) await cache.clearCache(id);
};
