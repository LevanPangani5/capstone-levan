import { body } from 'express-validator';

const feedValidation = [
  body('fromUser')
    .trim()
    .notEmpty()
    .isInt({ min: 1 })
    .withMessage('fromUser must be a positive number'),
  body('toUser')
    .trim()
    .notEmpty()
    .isInt({ min: 1 })
    .withMessage('userId must be a positive number'),
  body('companyName', 'Please Enter the company name')
    .trim()
    .notEmpty()
    .isString()
    .isLength({ max: 255 }),
  body('context', 'Please Enter the context')
    .trim()
    .notEmpty()
    .isString()
    .isLength({ max: 255 }),
];

export { feedValidation };
