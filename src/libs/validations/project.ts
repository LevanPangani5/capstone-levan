import { body } from 'express-validator';

const createProjectValidation = [
  body('userId', 'userId must be a positive number')
    .trim()
    .notEmpty()
    .isInt({ min: 1 })
    .withMessage('userId must be a positive number'),
  body('image').trim().optional().isString().isLength({ max: 256 }),
  body('description', 'Please enter the description')
    .trim()
    .notEmpty()
    .isString()
    .isLength({ max: 256 }),
];

const updateProjectValidation = [
  body('userId')
    .trim()
    .optional()
    .isInt({ min: 1 })
    .withMessage('userId must be a positive number'),
  body('image').trim().optional().isString().isLength({ max: 256 }),
  body('description').trim().optional().isString().isLength({ max: 256 }),
];

export { createProjectValidation, updateProjectValidation };
