import { body, query, param } from 'express-validator';
import { UserRole } from '../../models/user.model';

export const validRoles: string[] = Object.values(UserRole);

const createUserValidation = [
  body('firstName').trim().isString().notEmpty().isLength({ max: 128 }),
  body('lastName').trim().isString().notEmpty().isLength({ max: 128 }),
  body('title').trim().isString().notEmpty().isLength({ max: 128 }),
  body('summary').trim().isString().notEmpty().isLength({ max: 256 }),
  body('email').trim().isEmail().notEmpty(),
  body('password').trim().isLength({ min: 6 }).notEmpty(),
  body('role')
    .trim()
    .isString()
    .notEmpty()
    .custom((value) => {
      if (!validRoles.includes(value)) {
        throw new Error('Role must be User or Admin');
      }
      return true;
    }),
];

const paginationValidation = [
  query('pageSize')
    .trim()
    .notEmpty()
    .isInt({ min: 1 })
    .withMessage('pageSize must be  a positive number'),
  query('page')
    .trim()
    .notEmpty()
    .isInt({ min: 1 })
    .withMessage('page must be  a positive number'),
];

const idValidation = [
  param('id')
    .trim()
    .notEmpty()
    .isInt({ min: 1 })
    .withMessage('id must be a positive number'),
];
const userIdVallidation = [
  param('userId')
    .trim()
    .notEmpty()
    .isInt({ min: 1 })
    .withMessage('userId must be a positive number'),
];

const updateUserValidation = [
  body('firstName').trim().optional().isString().isLength({ max: 128 }),
  body('lastName').trim().optional().isString().isLength({ max: 128 }),
  body('title').trim().optional().isString().isLength({ max: 128 }),
  body('summary').trim().optional().isString().isLength({ max: 256 }),
  body('email').trim().optional().isEmail(),
  body('password').trim().optional().isLength({ min: 6 }),
  body('role').trim().optional().isString().isIn(validRoles),
];

export {
  createUserValidation,
  paginationValidation,
  idValidation,
  updateUserValidation,
  userIdVallidation,
};
