import { body } from 'express-validator';

const registrationValidation = [
  body('firstName', 'First name can be maximun 128 characters long')
    .trim()
    .isString()
    .notEmpty()
    .isLength({ max: 128 }),
  body('lastName', 'Last name can be maximun 128 characters long')
    .trim()
    .isString()
    .notEmpty()
    .isLength({ max: 128 }),
  body('title', 'Title can be maximun 256 characters long')
    .trim()
    .isString()
    .notEmpty()
    .isLength({ max: 128 }),
  body('summary', 'Summary can be maximun 256 characters long')
    .trim()
    .isString()
    .notEmpty()
    .isLength({ max: 256 }),
  body('email', 'Please enter a valid email adderss')
    .trim()
    .isEmail()
    .notEmpty(),
  body('password', 'Password must be at least 6 characters long')
    .trim()
    .isLength({ min: 6 })
    .notEmpty(),
];

const loginValidation = [
  body('email', 'Please enter a valid email adderss')
    .trim()
    .isEmail()
    .notEmpty(),
  body('password', 'Password must be at least 6 characters long')
    .trim()
    .isLength({ min: 6 })
    .notEmpty(),
];

export { registrationValidation, loginValidation };
