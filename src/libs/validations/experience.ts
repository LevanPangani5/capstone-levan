import { body } from 'express-validator';

const createExperienceValidation = [
  body('userId')
    .trim()
    .notEmpty()
    .isInt({ min: 1 })
    .withMessage('userId must be a positive number'),
  body('companyName', 'Please enter company name')
    .trim()
    .notEmpty()
    .isString()
    .isLength({ max: 255 }),
  body('role', 'Please enter your role at the company')
    .trim()
    .notEmpty()
    .isString()
    .isLength({ max: 255 }),
  body('startDate', 'Please enter the start date').trim().notEmpty().isDate(),
  body('endDate').trim().optional().isDate(),
  body('description').trim().optional().isString(),
];

const updateExperienceValidation = [
  body('userId')
    .trim()
    .optional()
    .isInt({ min: 1 })
    .withMessage('userId must be a positive number'),
  body('companyName').trim().optional().isString().isLength({ max: 255 }),
  body('role').trim().optional().isString().isLength({ max: 255 }),
  body('startDate').trim().optional().isDate(),
  body('endDate').trim().optional().isDate(),
  body('description').trim().optional().isString(),
];

export { createExperienceValidation, updateExperienceValidation };
