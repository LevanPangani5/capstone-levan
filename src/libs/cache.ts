import { createClient } from 'redis';

//class for handling caching
export class Cache {
  public client: ReturnType<typeof createClient>;

  constructor(config: { host: string; port: number }) {
    this.client = createClient({
      socket: config,
    });
  }

  async connect() {
    await this.client.connect();
  }

  async set(id: number, key: string) {
    await this.client.set(`${id}`, key);
  }

  async get(id: number) {
    return await this.client.get(`${id}`);
  }

  async clearCache(id: number) {
    await this.client.del(`${id}`);
  }

  async disconnect() {
    return await this.client.disconnect();
  }

  async quit() {
    return await this.client.quit();
  }
}
