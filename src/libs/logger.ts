import pino from 'pino';
export const logger = pino({
  transport: {
    target: 'pino-pretty',
    options: {
      colorize: true,
      translateTime: 'SYS:dd-mm-yyy HH:MM:ss',
      ignore: 'pid,hostname',
    },
  },
});
