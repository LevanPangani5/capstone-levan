import path from 'path';
import { v4 as uuidv4 } from 'uuid';
import fs from 'fs/promises';
import { validRoles } from './validations/user';
import { constants } from 'fs';
import { logger } from './logger';
import 'dotenv/config';

const defaults = [...validRoles, 'Project'];

//checks if file path is valid
async function fileExists(filePath: string): Promise<boolean> {
  try {
    await fs.access(filePath, constants.F_OK);
    return true;
  } catch (err) {
    return false;
  }
}
//delets file if it exists
async function deleteImage(imgPath: string) {
  if (await fileExists(imgPath)) {
    try {
      await fs.unlink(imgPath);
    } catch (err) {
      logger.error(err);
    }
  }
}
//generates default image if user did not entered file path
async function getDefaultImageFullPath(role: string): Promise<string> {
  const imageName = `${role}.png`;
  const publicDirectory = path.join(__dirname, '../../public');
  const imagePath = path.join(publicDirectory, imageName);
  //later we may add some roles but , we may not have default pictures for all of them so this code handles that
  const exists = await fileExists(imagePath);
  if (exists) {
    return path.join(publicDirectory, imageName);
  } else {
    const defaultImagePath = path.join(publicDirectory, 'User.png');
    return defaultImagePath;
  }
}
//generates default Profile image or image which's file Path was provided
async function uploadProfilePic(imgPath: string = 'User'): Promise<string> {
  const profilePicDirectory = path.join(__dirname, '../../public/profile');
  const extention = path.extname(imgPath) || '.png';
  const fileName = `image-profile-${uuidv4()}${extention}`;
  const profilePicturePath = path.join(profilePicDirectory, fileName);
  try {
    //checks if we have default image by this name 
    if (defaults.includes(imgPath)) {
      const defaultPath = await getDefaultImageFullPath(imgPath);
      await fs.copyFile(defaultPath, profilePicturePath);
    } else {
      await fs.copyFile(imgPath, profilePicturePath);
    }
    return profilePicturePath;
  } catch (err) {
    console.error(err);
    return err;
  }
}

//generates default Project image or image which's file Path was provided
async function uploadProjectPic(imgPath: string = 'Project'): Promise<string> {
  const projectPicDirectory = path.join(__dirname, '../../public/project');
  const extention = path.extname(imgPath) || '.png';
  const fileName = `image-project-${uuidv4()}${extention}`;
  const projectPicturePath = path.join(projectPicDirectory, fileName);
  try {
  //checks if we have default image by this name 
    if (defaults.includes(imgPath)) {
      const defaultPath = await getDefaultImageFullPath(imgPath);
      await fs.copyFile(defaultPath, projectPicturePath);
    } else {
      await fs.copyFile(imgPath, projectPicturePath);
    }
    return projectPicturePath;
  } catch (err) {
    console.error(err);
    return err;
  }
}

//when working with multer __dirname is in dist on runtime so i am using this function instead of it
function handleUploaded(imgPath: string) {
  return path.join(process.env.DIRNAME, imgPath);
}

export {
  uploadProfilePic,
  uploadProjectPic,
  handleUploaded,
  fileExists,
  deleteImage,
};
