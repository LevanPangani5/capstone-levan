import { Project } from '../models/project.model';
import { UserRole } from '../models/user.model';
import {
  deleteImage,
} from '../libs/imageHelper';
import { statusError } from '../middleware/handleErrors';
import { clearCache } from '../libs/clearCache';


export class ProjectService {
  addProject = async (
    userId: number, 
    image: string, 
    description: string) => {
  
    const project = await Project.create({
      userId,
      description,
      image: image,
    });
    await clearCache(project.userId);
    return {
      id: project.id,
      userId: project.userId,
      image: project.image,
      description: project.description,
    };  
  };

  getProjects = async (pageSize:number,page:number) => {
    const projects = await Project.paginateProjects(pageSize, page);
    return projects;
  };

  getProject = async (experienceId:number) => {  
    const project = await Project.findByPk(experienceId);
    if (!project) {
      throw new statusError('Project not found');
    }
    return {
      id: project.id,
      userId: project.userId,
      image: project.image,
      description: project.description,
    };
  };

  updateProject = async (
    projectId:number,
    currentUser:{ id: number; role: UserRole },
    userId: number, 
    image: string, 
    description: string) => {
    const project = await Project.findByPk(projectId);
    if (!project) throw new statusError('Project not Found');

    if (
      project.userId != currentUser.id ||
        (!isNaN(userId) &&
          currentUser.id != userId &&
          currentUser.role != UserRole.Admin)
    ) {
      throw new statusError('not allowed', 403);
    }
    project.description = description ?? project.description;
    if (image != null) {
      await deleteImage(project.image);
      project.image = image;
    }
    await project.save();
    await clearCache(project.userId);
    return{
      id: project.id,
      userId: project.userId,
      description: project.description,
      image: project.image,
    };
  };

  deleteProject = async (projectId:number, currentUser:{ id: number; role: UserRole }) => {
    const project = await Project.findByPk(projectId);
    if (!project){
      throw new statusError('project not found');
    }
    if (
      currentUser.id != project.userId &&
        currentUser.role != UserRole.Admin){
      throw new statusError('not Allowed', 403);
    }
    const userId = project.userId;
    await deleteImage(project.image);
    await project.destroy();
    await clearCache(userId);
  };
}
