import { Experience } from '../models/experience.model';
import { UserRole } from '../models/user.model';
import { statusError } from '../middleware/handleErrors';
import { clearCache } from '../libs/clearCache';

export class ExperienceService {
  addExperice = async (
    userId: number,
    companyName: string,
    role: string,
    startDate: Date,
    endDate: Date,
    description: string,
  ) => {
    const newExperience = await Experience.create({
      userId,
      companyName,
      role,
      startDate,
      endDate,
      description,
    });
    await clearCache(newExperience.userId);
    return {
      id: newExperience.id,
      userId: newExperience.userId,
      companyName: newExperience.companyName,
      startDate: newExperience.startDate,
      endDate: newExperience.endDate,
    };
  };

  getExperiences = async (pageSize:number, page:number) => {
    
    const experiences = await Experience.paginateExperiences(pageSize, page);
    return experiences;
  };

  getExperience = async (experienceId:number) => {
   
    const experience = await Experience.findByPk(experienceId);
    if (!experience) {
      throw new statusError('Experience not found');
    }
    return{
      id: experience.id,
      userId: experience.userId,
      companyName: experience.companyName,
      startDate: experience.startDate,
      endDate: experience.endDate,
    };
  };

  updateExperience = async (
    experienceId:number,
    currentUser:{
      id: number;
      role: UserRole;
  },
    userId: number,
    companyName: string,
    role: string,
    startDate: Date,
    endDate: Date,
    description: string
  ) => {
    const experience = await Experience.findByPk(experienceId);
    if (!experience) {
      throw new statusError('experience not found');
    }
      
    if (
      experience.userId != currentUser.id ||
        (currentUser.id != userId &&
          !isNaN(userId) &&
          currentUser.role != UserRole.Admin)
    ) {
      throw new statusError('not allowed', 403);
    }

    experience.companyName = companyName ?? experience.companyName;
    experience.role = role ?? experience.role;
    experience.startDate = startDate ?? experience.startDate;
    experience.endDate = endDate ?? experience.endDate;
    experience.description = description ?? experience.description;
    await experience.save();
    await clearCache(experience.userId);

    return {
      id: experience.id,
      userId: experience.userId,
      startDate: experience.startDate,
      companyName: experience.companyName,
      endDate: experience.endDate,
      description: experience.description,
    };
  };

  deleteExperience = async (
    experienceId:number,
    currentUser:{
      id: number;
      role: UserRole;
  },
  ) => {
    const experience = await Experience.findByPk(experienceId);
    if (!experience) {
      throw new statusError('experience not found ');
    }
      
    if (experience.userId != currentUser.id &&
         currentUser.role != UserRole.Admin) {
      throw new statusError(' not Allowed', 403);
    }
    const userId = experience.userId;
    await experience.destroy();
    await clearCache(userId);
    return userId;
  };
}
