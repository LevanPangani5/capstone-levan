import { User, UserRole, userFormat } from '../models/user.model';
import {
  deleteImage,
  uploadProfilePic,
} from '../libs/imageHelper';
import { Experience, experienceFormat } from '../models/experience.model';
import { Project, projectFormat } from '../models/project.model';
import { Feedback, feedbackFormat } from '../models/feedback.model';
import { statusError } from '../middleware/handleErrors';
import { logger } from '../libs/logger';
import { cache } from '../loaders/redis';
import { clearCache } from '../libs/clearCache';
import 'dotenv/config';

export class UserService {
  constructor() {}

  createUser = async (
    firstName: string,
    lastName: string,
    email: string,
    password: string,
    role: UserRole,
    title: string,
    summary: string,
    image: string) => {
    const oldUser = await User.findOne({ where: { email } });
    if (oldUser != undefined) {
      throw new statusError('User With this email already exists', 400);
    }
    const newUser = await User.create({
      firstName,
      lastName,
      title,
      summary,
      email,
      password,
      role: role,
      image,
    });

    return {
      message: 'New user was registered',
      user: {
        id: newUser.id,
        firstName: newUser.firstName,
        lastName: newUser.lastName,
        title: newUser.title,
        summary: newUser.summary,
        email: newUser.email,
        role: newUser.role,
      }
    };
  };

  getUsers = async (pageSize:number, page: number) => {
    const users = await User.paginateUsers(pageSize, page);  
    return users;
  };

  getUser = async (userId:number) => {
    const user = await User.findByPk(userId);

    if (!user) throw new statusError('user not found');

    return {
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      title: user.title,
      summary: user.summary,
      email: user.email,
      password: user.password,
    }; 
  };

  updateUser = async (
    userId:number,
    firstName: string,
    lastName: string,
    email: string,
    password: string,
    role: UserRole,
    title: string,
    summary: string,
    image: string) => {
    const user = await User.findByPk(userId);
    if (!user) {
      throw new statusError('user not found');
    }

    if (image != null) {
      await deleteImage(user.image);
      user.image = image;
    }
    if (firstName) {
      user.firstName = firstName;
    }
    if (lastName) {
      user.lastName = lastName;
    }
    if (title) {
      user.title = title;
    }
    if (summary) {
      user.summary = summary;
    }
    if (email !== undefined) {
      if (email !== user.email) {
        const checkEmail = await User.findOne({ where: { email } });
        if (checkEmail) {
          logger.warn(
            'Email user entered already exists so we are not changing the old one',
          );
        } else {
          user.email = email;
        }
      }
    }
    if (role) {
      if (role !== user.role) {
        user.role = role;
        if (image != null) {
          await deleteImage(user.image);
          user.image = await uploadProfilePic(role);
        }
      }
    }
    if (password) {
      user.password = password;
    }

    await user.save();
    await clearCache(userId);
    return{
      message: ' user was updated',
      user: {
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        title: user.title,
        summary: user.summary,
        role: user.role,
        password: user.password,
      },
    };
  };

  deleteUser = async (userId:number) => {
    const user = await User.findByPk(userId);
    if (!user) {
      throw new statusError('user not found');
    }
    await deleteImage(user.image);
    await user.destroy();
    await clearCache(userId);
  };

  getCV = async (id:number) => {
    if (cache != null) {
      const cachedCV = await cache.get(id);
      if (cachedCV !=null) {
        logger.info(cachedCV);
        return JSON.parse(cachedCV);
      }
    }
      
    const user = await User.findByPk(id, {
      attributes: userFormat.slice(0, userFormat.length - 1),
    });
    if (!user) {
      throw new statusError('user not found');
    }

    const experiences = await Experience.findAll({
      where: { userId: id },
      attributes: experienceFormat,
    });

    const projects = await Project.findAll({
      where: { userId: id },
      attributes: projectFormat,
    });

    const feedbacks = await Feedback.findAll({
      where: { toUser: id },
      attributes: feedbackFormat,
    });
    const cv = {
      user,
      experiences,
      projects,
      feedbacks,
    };

    if (cache) await cache.set(id, JSON.stringify(cv));
    return {cv};
  };
}
