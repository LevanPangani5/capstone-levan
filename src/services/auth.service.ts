import { User, UserRole } from '../models/user.model';
import { statusError } from '../middleware/handleErrors';

export class AuthService {

  register = async ( 
    firstName: string,
    lastName: string,
    title: string,
    summary: string,
    email: string,
    password: string,
    role:UserRole,
    image:string,
  ) => {
    // registration logic
  
    const oldUser = await User.findOne({ where: { email } });
    if (oldUser) {
      throw new statusError('User With this email already exists', 400);
    }
    const newUser = await User.create({
      firstName,
      lastName,
      title,
      summary,
      email,
      password,
      role,
      image,
    });

    return {
      id: newUser.id,
      firstName: newUser.firstName,
      lastName: newUser.lastName,
      title: newUser.title,
      summary: newUser.summary,
      email: newUser.email,
      image: newUser.image,
    };
  };
}
