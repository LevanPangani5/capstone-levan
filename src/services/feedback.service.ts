import { Feedback } from '../models/feedback.model';
import { UserRole } from '../models/user.model';
import { User } from '../models/user.model';
import { statusError } from '../middleware/handleErrors';
import { clearCache } from '../libs/clearCache';

export class FeedbackService {
  addFeedback = async (  
    currentUserId:number,
    fromUser: number,
    toUser: number,
    context: string,
    companyName: string,
  ) => {
    const toUserObj = await User.findByPk(toUser);

    if (!toUserObj) throw new statusError('toUser not found', 404);

    if (fromUser != currentUserId) {
      throw new statusError('not allowed', 403);
    }
    const newFeedback = await Feedback.create({
      fromUser,
      toUser,
      companyName,
      context,
    });
    await clearCache(newFeedback.toUser);
    return{
      id: newFeedback.id,
      fromUser: newFeedback.fromUser,
      toUser: newFeedback.toUser,
      companyName: newFeedback.companyName,
      context: newFeedback.context,
    };
  };

  getFeedbacks = async (pageSize:number, page:number) => {
    const feedbacks = await Feedback.paginateFeedbacks(pageSize, page);
     
    return feedbacks;
  };

  getFeedback = async (feedbackId:number) => {
    const feedback = await Feedback.findByPk(feedbackId);
    if (!feedback) {
      throw new statusError('Feedback with such id does not exist');
    }
    return {
      fromUser: feedback.fromUser,
      toUser: feedback.toUser,
      companyName: feedback.companyName,
      context: feedback.context,
    };
  };

  updateFeedback = async ( 
    feedbackId:number,
    currentUser:{ id: number; role: UserRole },
    fromUser: number,
    toUser: number,
    context: string,
    companyName: string,
  ) => {
 
    const feedback = await Feedback.findByPk(feedbackId);
    const toUserObj = await User.findByPk(toUser);
    if (!toUserObj || !feedback)
      throw new statusError('user or feedback not found');

    if (
      feedback.fromUser != currentUser.id &&
        currentUser.id != fromUser &&
        !isNaN(fromUser) &&
        currentUser.role != UserRole.Admin
    )
      throw new statusError('not Allowed', 403);

    feedback.fromUser = Number(fromUser);
    feedback.toUser = Number(toUser);
    feedback.companyName = companyName;
    feedback.context = context;
    await feedback.save();
    await clearCache(feedback.toUser);
    return {
      id: feedback.id,
      fromUser: feedback.fromUser,
      companyName: feedback.companyName,
      toUser: feedback.toUser,
      context: context,
    };
  };

  deleteFeedback = async (feedbackId:number,currentUser:{ id: number; role: UserRole }) => {
    const feedback = await Feedback.findByPk(feedbackId);
    if (!feedback) {
      throw new statusError('feedback not found');
    }

    if (
      feedback.fromUser != currentUser.id &&
        currentUser.role != UserRole.Admin
    )
      throw new statusError('not allowed', 403);
    const userId = feedback.toUser;
    await feedback.destroy();
    await clearCache(userId);
  };
}
