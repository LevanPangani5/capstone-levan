import supertest from 'supertest';
import { loadApp } from '../loaders/app';
import { cache } from '../loaders/redis';

let request: ReturnType<typeof supertest>;

let adminToken: string;
let secondUserId: string;
let adminId: string;
beforeAll(async () => {
  const app = await loadApp();

  request = supertest(app);

  const res = await request
    .post('/api/auth/login')
    .field('email', 'admin@gmail.com')
    .field('password', 'pass1234');

  adminToken = res.body.token;
  adminId = res.body.user.id;
  await request
    .post('/api/auth/register')
    .field('email', 'test2@gmail.com')
    .field('password', 'pass1234')
    .field('firstName', 'test')
    .field('lastName', 'test')
    .field('title', 'title')
    .field('summary', 'summary');
});

afterAll(async () => {
  await cache.quit();
});

describe('POST /users', () => {
  it('should return 400 when email is already in use', async () => {
    const res = await request
      .post('/api/users')
      .field('email', 'admin@gmail.com')
      .field('password', 'testpassword')
      .field('firstName', 'Test')
      .field('lastName', 'test')
      .field('title', 'title')
      .field('summary', 'summary')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it('should return 201 when the input is correct and the request is from an admin', async () => {
    const res = await request
      .post('/api/users')
      .field('email', 'admin2@gmail.com')
      .field('password', 'pass1234')
      .field('firstName', 'test')
      .field('lastName', 'test')
      .field('title', 'title')
      .field('summary', 'summary')
      .field('role', 'User')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(201);
    secondUserId = res.body.user.id;
  });
});

describe('GET /users', () => {
  it('should return 200 when the request is from a admin', async () => {
    const res = await request
      .get('/api/users')
      .set('Authorization', 'Bearer ' + adminToken)
      .query({ pageSize: 1, page: 1 });

    expect(res.statusCode).toEqual(200);
  });
});

describe('GET /:id', () => {
  it('should return 400 if the id is invalid', async () => {
    const res = await request.get('/api/users/-1');

    expect(res.statusCode).toEqual(400);
  });

  it('should return 404 if the user cannot be found', async () => {
    const res = await request.get('/api/users/555');

    expect(res.statusCode).toEqual(404);
  });

  it('should return 200 if the user exists', async () => {
    const res = await request.get('/api/users/' + adminId);

    expect(res.statusCode).toEqual(200);
  });
});

describe('PUT /:id', () => {
  it('should return 200 when everything is in order', async () => {
    const res = await request
      .put('/api/users/' + adminId)
      .field('lastName', 'edited')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(200);
  });

  it('should return 404 when user with given id does not exist', async () => {
    const res = await request
      .put('/api/users/555')
      .field('email', 'admin@gmail.com')
      .field('password', 'pass1234')
      .field('role', 'Admin')
      .field('summary', 'summary')
      .field('title', 'title')
      .field('firstName', 'Admin')
      .field('lastName', 'Admin')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(404);
  });
});

describe('DELETE /:id', () => {
  let userToken: string;
  let userId: string;

  beforeAll(async () => {
    const res = await request
      .post('/api/auth/login')
      .field('email', 'test2@gmail.com')
      .field('password', 'pass1234');

    userToken = res.body.token;
    userId = res.body.user.id;
  });

  it('should return 404 if the user does not exist', async () => {
    const res = await request
      .delete('/api/users/555')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(404);
  });

  it('should return 403 if a normal user is trying to delete somebody elses account', async () => {
    const res = await request
      .delete('/api/users/' + adminId)
      .set('Authorization', 'Bearer ' + userToken);

    expect(res.statusCode).toEqual(403);
  });

  it('should return 204 if the user was successfully deleted', async () => {
    const res = await request
      .delete('/api/users/' + userId)
      .set('Authorization', 'Bearer ' + userToken);

    expect(res.statusCode).toEqual(204);
  });

  it('should return 204 if an admin is deleting another user', async () => {
    const res = await request
      .delete('/api/users/' + secondUserId)
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(204);
  });
});
