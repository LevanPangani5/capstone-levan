import supertest from 'supertest';
import { loadApp } from '../loaders/app';
import { cache } from '../loaders/redis';


let request: ReturnType<typeof supertest>;

let adminId: string;
beforeAll(async () => {
  const app = await loadApp();
  request = supertest(app);

  const res = await request
    .post('/api/auth/login')
    .field('email', 'admin@gmail.com')
    .field('password', 'pass1234');

  adminId = res.body.user.id;
});

afterAll(async () => {
  await cache.quit();
});

describe('GET /cv', () => {
  it('should return 400 if id is invalid', async () => {
    const res = await request.get('/api/users/5.55/cv');

    expect(res.statusCode).toEqual(400);
  });

  it('should return 404 if user does not exist', async () => {
    const res = await request.get('/api/users/555/cv');

    expect(res.statusCode).toEqual(404);
  });

  it('should return 200 if everything is in order', async () => {
    const res = await request.get('/api/users/' + adminId + '/cv');

    expect(res.statusCode).toEqual(200);
  });

  it('should fetch feedbacks, projects and experiences when a user exists', async () => {
    const res = await request.get('/api/users/' + adminId + '/cv');

    expect(res.body).toHaveProperty('user');
    expect(res.body).toHaveProperty('projects');
    expect(res.body).toHaveProperty('feedbacks');
    expect(res.body).toHaveProperty('experiences');
  });
});
