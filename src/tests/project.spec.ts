import supertest from 'supertest';
import { loadApp } from '../loaders/app';
import { cache } from '../loaders/redis';

let request: ReturnType<typeof supertest>;

let adminToken: string;
let adminId: string;
let userId: string;
let userToken: string;
let projectId: string = '1';

beforeAll(async () => {
  const app = await loadApp();

  request = supertest(app);

  const res = await request
    .post('/api/auth/login')
    .field('email', 'admin@gmail.com')
    .field('password', 'pass1234');

  adminToken = res.body.token;
  adminId = res.body.user.id;
});

afterAll(async () => {
  await request
    .delete('/api/users/' + userId)
    .set('Authorization', 'Bearer ' + userToken);
  await cache.quit();
});

describe('POST /projects', () => {
  beforeAll(async () => {
    await request
      .post('/api/auth/register')
      .field('email', 'test5@gmail.com')
      .field('password', 'pass1234')
      .field('firstName', 'Test')
      .field('lastName', 'test')
      .field('title', 'title')
      .field('summary', 'summary');

    const res = await request
      .post('/api/auth/login')
      .field('email', 'test5@gmail.com')
      .field('password', 'pass1234');

    userId = res.body.user.id;
    userToken = res.body.token;
  });

  it('should return 400 if fields are missing', async () => {
    const res = await request
      .post('/api/projects')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it('should return 403 if ids dont match', async () => {
    const res = await request
      .post('/api/projects')
      .set('Authorization', 'Bearer ' + adminToken)
      .field('userId', '500')
      .field('description', 'description');

    console.log(res.body);
    expect(res.statusCode).toEqual(403);
  });

  it('should return 201 when everything is in order', async () => {
    const res = await request
      .post('/api/projects')
      .set('Authorization', 'Bearer ' + adminToken)
      .field('userId', adminId)
      .field('description', 'description');

    expect(res.statusCode).toEqual(201);
    projectId = res.body.id;
  });
});

describe('GET /projects', () => {
  it('should return 403 if a normal user sends a request', async () => {
    const res = await request
      .get('/api/projects')
      .set('Authorization', 'Bearer ' + userToken)
      .query({ pageSize: 1, page: 1 });
    expect(res.statusCode).toEqual(403);
  });

  it('should return 400 if query parameters are missing', async () => {
    const res = await request
      .get('/api/projects')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it('should return 200 if everything is in order', async () => {
    const res = await request
      .get('/api/projects')
      .set('Authorization', 'Bearer ' + adminToken)
      .query({
        pageSize: 1,
        page: 1,
      });

    expect(res.statusCode).toEqual(200);
  });
});

describe('GET /:id', () => {
  it('should return 400 if id is invalid', async () => {
    const res = await request.get('/api/projects/-1');

    expect(res.statusCode).toEqual(400);
  });

  it('should return 404 if the project does not exist', async () => {
    const res = await request.get('/api/projects/555');

    expect(res.statusCode).toEqual(404);
  });

  it('should return 200 if everything is in order', async () => {
    const res = await request.get('/api/projects/' + projectId);

    expect(res.statusCode).toEqual(200);
  });
});

describe('PUT /:id', () => {
  it('should return 400 if id is invalid', async () => {
    const res = await request
      .put('/api/projects/55.5')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 403 if a user is trying to edit another user's projects", async () => {
    const res = await request
      .put('/api/projects/' + projectId)
      .set('Authorization', 'Bearer ' + userToken)
      .field('userId', '555')
      .field('description', 'description');

    expect(res.statusCode).toEqual(403);
  });

  it('should return 404 if the project does not exist', async () => {
    const res = await request
      .put('/api/projects/555')
      .set('Authorization', 'Bearer ' + adminToken)
      .field('userId', adminId)
      .field('description', 'description');

    expect(res.statusCode).toEqual(404);
  });

  it('should return 403 if ids dont match', async () => {
    const res = await request
      .put('/api/projects/' + projectId)
      .set('Authorization', 'Bearer ' + userToken)
      .field('userId', userId)
      .field('description', 'someDescirption');

    expect(res.statusCode).toEqual(403);
  });

  it('should return 200 if everything is in order', async () => {
    const res = await request
      .put('/api/projects/' + projectId)
      .set('Authorization', 'Bearer ' + adminToken)
      .field('userId', adminId)
      .field('description', 'updated description');

    expect(res.statusCode).toEqual(200);
  });
});

describe('DETELE /:id', () => {
  let userProjectId: string;
  beforeAll(async () => {
    const res = await request
      .post('/api/projects')
      .set('Authorization', 'Bearer ' + userToken)
      .field('userId', userId)
      .field('description', 'description');
    userProjectId = res.body.id;
  });

  it('should return 400 if id is invalid', async () => {
    const res = await request
      .delete('/api/projects/55.5')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it('should return 404 if project does not exist', async () => {
    const res = await request
      .delete('/api/projects/555')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(404);
  });

  it("should return 403 if a user is trying to delete another user's project", async () => {
    const res = await request
      .delete('/api/projects/' + projectId)
      .set('Authorization', 'Bearer ' + userToken);

    expect(res.statusCode).toEqual(403);
  });

  it("should return 204 if an admin deletes another user's project", async () => {
    const res = await request
      .delete('/api/projects/' + userProjectId)
      .set('Authorization', 'Bearer ' + adminToken);
    expect(res.statusCode).toEqual(204);
  });

  it('should return 204 if a user is trying to delete their own project', async () => {
    const res = await request
      .delete('/api/projects/' + projectId)
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(204);
  });
});
