import supertest from 'supertest';
import { loadApp } from '../loaders/app';
import { cache } from '../loaders/redis';

let request: ReturnType<typeof supertest>;

let adminToken: string;
let adminId: string;
let userId: string;
let userToken: string;
let experienceId: string = '1';

beforeAll(async () => {
  const app = await loadApp();

  request = supertest(app);

  const res = await request
    .post('/api/auth/login')
    .field('email', 'admin@gmail.com')
    .field('password', 'pass1234');

  adminToken = res.body.token;
  adminId = res.body.user.id;
});

afterAll(async () => {
  await request
    .delete('/api/users/' + userId)
    .set('Authorization', 'Bearer ' + userToken);
  await cache.quit();
});

describe('POST /experience', () => {
  beforeAll(async () => {
    await request
      .post('/api/auth/register')
      .field('email', 'test3@gmail.com')
      .field('password', 'pass1234')
      .field('firstName', 'test')
      .field('lastName', 'test')
      .field('title', 'title')
      .field('summary', 'summary');

    const res = await request
      .post('/api/auth/login')
      .field('email', 'test3@gmail.com')
      .field('password', 'pass1234');

    userId = res.body.user.id;
    userToken = res.body.token;
  });

  it('should return 400 when required fields are missing', async () => {
    const res = await request
      .post('/api/experience')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it("should return 403 if the ids don't match", async () => {
    const res = await request
      .post('/api/experience')
      .field('userId', userId)
      .field('companyName', 'test')
      .field('role', 'role')
      .field('startDate', '2015-05-05')
      .field('endDate', '2045-05-05')
      .field('description', 'test des')
      .set('Authorization', 'Bearer ' + adminToken);
    expect(res.statusCode).toEqual(403);
  });

  it('should return 201 if the experience was created', async () => {
    const res = await request
      .post('/api/experience')
      .field('userId', adminId)
      .field('companyName', 'company')
      .field('role', 'role')
      .field('startDate', '2025-05-05')
      .field('description', 'test des')
      .set('Authorization', 'Bearer ' + adminToken);
    expect(res.statusCode).toEqual(201);
    experienceId = res.body.id;
  });
});

describe('GET /experience', () => {
  it('should return 403 when a  user is sending a request', async () => {
    const res = await request
      .get('/api/experience')
      .set('Authorization', 'Bearer ' + userToken)
      .query({ pageSize: 1, page: 1 });

    expect(res.statusCode).toEqual(403);
  });

  it('should return 400 when query parameters are missing', async () => {
    const res = await request
      .get('/api/experience')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it('should return 200 if everything is in order', async () => {
    const res = await request
      .get('/api/experience')
      .set('Authorization', 'Bearer ' + adminToken)
      .query({
        pageSize: 1,
        page: 1,
      });

    expect(res.statusCode).toEqual(200);
  });
});

describe('GET /:id', () => {
  it('should return 404 if experience does not exist', async () => {
    const res = await request.get('/api/experience/555');

    expect(res.statusCode).toEqual(404);
  });

  it('should return 400 if id is invalid', async () => {
    const res = await request.get('/api/experience/55.5');

    expect(res.statusCode).toEqual(400);
  });

  it('should return 200 if experience exists', async () => {
    const res = await request.get('/api/experience/' + experienceId);

    expect(res.statusCode).toEqual(200);
  });
});

describe('PUT /:id', () => {
  it('should return 404 if experience does not exist', async () => {
    const res = await request
      .put('/api/experience/500')
      .set('Authorization', 'Bearer ' + adminToken)
      .field('userId', '555')
      .field('companyName', 'company')
      .field('role', 'role')
      .field('startDate', '2015-05-05')
      .field('endDate', '2015-05-05')
      .field('description', 'test des');
    expect(res.statusCode).toEqual(404);
  });

  it('should return 400 if user ids do not match', async () => {
    const res = await request
      .put('/api/experience/' + experienceId)
      .field('userId', adminId)
      .field('companyName', 'company')
      .field('role', 'role')
      .field('startDate', '2015-05-05')
      .field('endDate', '2015-05-05')
      .field('description', 'test des')
      .set('Authorization', 'Bearer ' + userToken);
    expect(res.statusCode).toEqual(403);
  });

  it('should return 200 if everything is in order', async () => {
    const res = await request
      .put('/api/experience/' + experienceId)
      .set('Authorization', 'Bearer ' + adminToken)
      .field('userId', adminId)
      .field('companyName', 'company')
      .field('role', 'role')
      .field('startDate', '2011-04-05')
      .field('endDate', '2016-09-05')
      .field('description', 'test des');

    expect(res.statusCode).toEqual(200);
  });
});

describe('DELETE /:id', () => {
  let expId: string;
  let secondexpId: string;

  beforeAll(async () => {
    const res = await request
      .post('/api/experience')
      .set('Authorization', 'Bearer ' + userToken)
      .field('userId', userId)
      .field('companyName', 'company')
      .field('role', 'role')
      .field('startDate', '2012-06-05')
      .field('endDate', '2012-09-04')
      .field('description', 'test des');

    expId = res.body.id;
    const secondRes = await request
      .post('/api/experience')
      .set('Authorization', 'Bearer ' + userToken)
      .field('userId', userId)
      .field('companyName', 'comppany')
      .field('role', 'role')
      .field('startDate', '2012-06-05')
      .field('endDate', '2012-09-04')
      .field('description', 'test des');

    secondexpId = secondRes.body.id;
  });

  it('should return 400 if id is invalid', async () => {
    const res = await request
      .delete('/api/experience/xe')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it('should return 404 if experience cannot be found', async () => {
    const res = await request
      .delete('/api/experience/555')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(404);
  });

  it("should return 403 if a user is trying to delete another user's experience", async () => {
    const res = await request
      .delete('/api/experience/' + experienceId)
      .set('Authorization', 'Bearer ' + userToken);

    expect(res.statusCode).toEqual(403);
  });

  it("should return 204 when an admin deletes another user's experience", async () => {
    const res = await request
      .delete('/api/experience/' + expId)
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(204);
  });

  it('should return 204 when a user deletes their own experience', async () => {
    const res = await request
      .delete('/api/experience/' + secondexpId)
      .set('Authorization', 'Bearer ' + userToken);

    expect(res.status).toEqual(204);
  });

  it('should return 204 when a admin deletes their own experience', async () => {
    const res = await request
      .delete('/api/experience/' + experienceId)
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.status).toEqual(204);
  });
});
