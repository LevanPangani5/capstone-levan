import supertest from 'supertest';
import { loadApp } from '../loaders/app';
import { cache } from '../loaders/redis';

let request: ReturnType<typeof supertest>;
let userId: string;
let userToken: string;
beforeAll(async () => {
  const app = await loadApp();
  request = supertest(app);
});

afterAll(async () => {
  await request
    .delete('/api/users/' + userId)
    .set('Authorization', 'Bearer ' + userToken);
  await cache.quit();
});

describe('POST /register', () => {
  it('should return 400 when password is too short', async () => {
    const res = await request
      .post('/api/auth/register')
      .field('email', 'test@gmail.com')
      .field('password', 'pass')
      .field('firstName', 'Test')
      .field('lastName', 'test')
      .field('title', 'title')
      .field('summary', 'summary');

    expect(res.statusCode).toEqual(400);
  });

  it('should return 201 when everything is correct', async () => {
    const res = await request
      .post('/api/auth/register')
      .field('email', 'test@gmail.com')
      .field('password', 'pass1234')
      .field('firstName', 'Test')
      .field('lastName', 'test')
      .field('title', 'title')
      .field('summary', 'summary');
    expect(res.statusCode).toEqual(201);
  });

  it('should return 400 when email is already in use', async () => {
    const res = await request
      .post('/api/auth/register')
      .field('email', 'test@gmail.com')
      .field('password', 'pass1234')
      .field('firstName', 'Test1')
      .field('lastName', 'test1')
      .field('title', 'title')
      .field('summary', 'summary');

    expect(res.statusCode).toEqual(400);
  });
});

describe('POST /login', () => {
  it('should return 401 when email or password are incorrect', async () => {
    const res = await request
      .post('/api/auth/login')
      .field('email', 'test@gmail.com')
      .field('password', 'incorrect');

    expect(res.statusCode).toEqual(401);
  });

  it('should return 200 when credentials are correct', async () => {
    const res = await request
      .post('/api/auth/login')
      .field('email', 'test@gmail.com')
      .field('password', 'pass1234');
    userId = res.body.user.id;
    userToken = res.body.token;
    expect(res.statusCode).toEqual(200);
  });
});
