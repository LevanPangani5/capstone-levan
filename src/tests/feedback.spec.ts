import supertest from 'supertest';
import { loadApp } from '../loaders/app';
import { cache } from '../loaders/redis';

let request: ReturnType<typeof supertest>;

let adminToken: string;
let adminId: string;
let userId: string;
let userToken: string;
let testFeedbackId: string = '1';

beforeAll(async () => {
  const app = await loadApp();

  request = supertest(app);

  const res = await request
    .post('/api/auth/login')
    .field('email', 'admin@gmail.com')
    .field('password', 'pass1234');

  adminToken = res.body.token;
  adminId = res.body.user.id;
});

afterAll(async () => {
  await request
    .delete('/api/users/' + userId)
    .set('Authorization', 'Bearer ' + userToken);
  await cache.quit();
});

describe('POST /feedback', () => {
  beforeAll(async () => {
    await request
      .post('/api/auth/register')
      .field('email', 'test4@gmail.com')
      .field('password', 'pass1234')
      .field('firstName', 'test')
      .field('lastName', 'test')
      .field('title', 'title')
      .field('summary', 'summary');

    const res = await request
      .post('/api/auth/login')
      .field('email', 'test4@gmail.com')
      .field('password', 'pass1234');

    userId = res.body.user.id;
    userToken = res.body.token;
  });

  it('should return 400 if a field is missing', async () => {
    const res = await request
      .post('/api/feedback')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it('should return 404 if a toUser does not exist', async () => {
    const res = await request
      .post('/api/feedback')
      .set('Authorization', 'Bearer ' + adminToken)
      .field('fromUser', adminId)
      .field('toUser', '400')
      .field('companyName', 'company')
      .field('context', 'context');

    expect(res.statusCode).toEqual(404);
  });

  it("should return 403 if try to add feedback by other's id ", async () => {
    const res = await request
      .post('/api/feedback')
      .set('Authorization', 'Bearer ' + adminToken)
      .field('fromUser', userId)
      .field('toUser', adminId)
      .field('companyName', 'company')
      .field('context', 'context');

    expect(res.statusCode).toEqual(403);
  });

  it('should return 201 if everything is in order', async () => {
    const res = await request
      .post('/api/feedback')
      .set('Authorization', 'Bearer ' + adminToken)
      .field('fromUser', adminId)
      .field('toUser', userId)
      .field('companyName', 'company')
      .field('context', 'context');

    expect(res.statusCode).toEqual(201);
    testFeedbackId = res.body.id;
  });
});

describe('GET /feedback', () => {
  it('should return 403 if a normal user is sending a request', async () => {
    const res = await request
      .get('/api/feedback')
      .set('Authorization', 'Bearer ' + userToken);

    expect(res.statusCode).toEqual(403);
  });

  it('should return 400 if queries are missing', async () => {
    const res = await request
      .get('/api/feedback')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it('should return 200 if everything is in order', async () => {
    const res = await request
      .get('/api/feedback')
      .set('Authorization', 'Bearer ' + adminToken)
      .query({
        pageSize: 1,
        page: 1,
      });

    expect(res.statusCode).toEqual(200);
  });
});

describe('GET /:id', () => {
  it('should return 400 if id is invalid', async () => {
    const res = await request.get('/api/feedback/55.5');

    expect(res.statusCode).toEqual(400);
  });

  it('should return 404 if feedback does not exist', async () => {
    const res = await request.get('/api/feedback/555');

    expect(res.statusCode).toEqual(404);
  });

  it('should return 200 if everything is in order', async () => {
    const res = await request.get('/api/feedback/' + testFeedbackId);

    expect(res.statusCode).toEqual(200);
  });
});

describe('PUT /:id', () => {
  it('should return 400 if id is invalid', async () => {
    const res = await request
      .put('/api/feedback/55.5')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it('should return 404 if feedback does not exist', async () => {
    const res = await request
      .put('/api/feedback/555')
      .set('Authorization', 'Bearer ' + adminToken)
      .field('fromUser', adminId)
      .field('toUser', userId)
      .field('context', 'context')
      .field('companyName', 'company');

    expect(res.statusCode).toEqual(404);
  });

  it('should return 404 if to user cannot be found', async () => {
    const res = await request
      .put('/api/feedback/' + testFeedbackId)
      .set('Authorization', 'Bearer ' + adminToken)
      .field('fromUser', adminId)
      .field('toUser', '555')
      .field('context', 'context')
      .field('companyName', 'company');

    expect(res.statusCode).toEqual(404);
  });

  it('should return 403 if ids do not match', async () => {
    const res = await request
      .put('/api/feedback/' + testFeedbackId)
      .set('Authorization', 'Bearer ' + userToken)
      .field('fromUser', adminId)
      .field('toUser', userId)
      .field('context', 'context')
      .field('companyName', 'company');

    expect(res.statusCode).toEqual(403);
  });

  it('should return 200 if everything is in order', async () => {
    const res = await request
      .put('/api/feedback/' + testFeedbackId)
      .set('Authorization', 'Bearer ' + adminToken)
      .field('fromUser', adminId)
      .field('toUser', userId)
      .field('context', 'context')
      .field('companyName', 'company');
    expect(res.statusCode).toEqual(200);
  });
});

describe('DELETE /:id', () => {
  let feedbackId: string;
  beforeAll(async () => {
    const res = await request
      .post('/api/feedback')
      .set('Authorization', 'Bearer ' + userToken)
      .field('fromUser', userId)
      .field('toUser', adminId)
      .field('companyName', 'company')
      .field('context', 'context');

    feedbackId = res.body.id;
  });

  it('should return 400 if id is invalid', async () => {
    const res = await request
      .delete('/api/feedback/55.5')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(400);
  });

  it('should return 404 if feedback does not exist', async () => {
    const res = await request
      .delete('/api/feedback/555')
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(404);
  });

  it("should return 403 if a user is trying to delete another user's feedback", async () => {
    const res = await request
      .delete('/api/feedback/' + testFeedbackId)
      .set('Authorization', 'Bearer ' + userToken);

    expect(res.statusCode).toEqual(403);
  });

  it("should return 204 if an admin is deleting another user's feedback", async () => {
    const res = await request
      .delete('/api/feedback/' + feedbackId)
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(204);
  });

  it('should return 204 if an user is deleting their own feedback', async () => {
    const res = await request
      .delete('/api/feedback/' + testFeedbackId)
      .set('Authorization', 'Bearer ' + adminToken);

    expect(res.statusCode).toEqual(204);
  });
});
