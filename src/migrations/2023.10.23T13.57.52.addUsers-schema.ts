import { MigrationFn } from 'umzug';
import { Sequelize } from 'sequelize';

import { uploadProfilePic } from '../libs/imageHelper';
import bcrypt from 'bcrypt';
import 'dotenv/config';

export const up: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();
  const hashedPassword = await bcrypt.hash(
    process.env.DEFAULT_PWD,
    +process.env.BCRYPT_SALT,
  );

  // Insert an admin user
  await q.bulkInsert('users', [
    {
      first_name: 'John',
      last_name: 'Doe',
      image: await uploadProfilePic('Admin'),
      title: 'Administrator',
      summary: 'Not a regular user',
      role: 'Admin',
      email: 'admin@gmail.com',
      password: hashedPassword,
      created_at: new Date(),
      updated_at: new Date(),
    },
  ]);

  await q.bulkInsert('users', [
    {
      first_name: 'Luka',
      last_name: 'Pangani',
      image: await uploadProfilePic(),
      title: 'cuxi',
      summary: 'just ordinary cuxi',
      role: 'User',
      email: 'user@gmail.com',
      password: hashedPassword,
      created_at: new Date(),
      updated_at: new Date(),
    },
  ]);
};

export const down: MigrationFn<Sequelize> = async ({ context }) => {
  const q = context.getQueryInterface();
  await q.bulkDelete('users', { email: 'admin@gmail.com' });
  await q.bulkDelete('users', { email: 'user@gmail.com' });
};
