import { ControllerContext, RouterFactory } from '../interfaces/general';
import express, {Response,Request, NextFunction} from 'express';
import { UserRole } from '../models/user.model';
import { runValidation } from '../middleware/runValidation';
import roles from '../middleware/authorization';
import { idValidation, paginationValidation } from '../libs/validations/user';
import isAuthenticated from '../middleware/authentication';
import {
  updateProjectValidation,
  createProjectValidation,
} from '../libs/validations/project';
export const makeProjectRouter: RouterFactory = (context: ControllerContext) => {
  const router = express.Router();
  const project = context.controllers.projectController;

  router.post(
    '/',
    isAuthenticated,
    roles([UserRole.Admin, UserRole.User]),
    createProjectValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await project.addProject(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.get(
    '/',
    isAuthenticated,
    roles([UserRole.Admin]),
    paginationValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await project.getProjects(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.get('/:id', idValidation, runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await project.getProject(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.put(
    '/:id',
    isAuthenticated,
    idValidation,
    updateProjectValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await project.updateProject(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.delete(
    '/:id',
    isAuthenticated,
    idValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await project.deleteProject(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  return router;
};
