import {ControllerContext, RouterFactory } from '../interfaces/general';
import express, {Request, Response, NextFunction} from 'express';
import isAuthenticated from '../middleware/authentication';
import roles from '../middleware/authorization';
import ownsAccount from '../middleware/ownsAccount';
import { UserRole } from '../models/user.model';
import {
  createUserValidation,
  paginationValidation,
  idValidation,
  userIdVallidation,
  updateUserValidation,
} from '../libs/validations/user';
import { runValidation } from '../middleware/runValidation';
import canAccess from '../middleware/canAccess';
export const makeUserRouter: RouterFactory = (context: ControllerContext) => {
  const router = express.Router();
  const user = context.controllers.userController;

  router.post(
    '/',
    isAuthenticated,
    roles([UserRole.Admin]),
    createUserValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await user.createUser(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.get(
    '/',
    isAuthenticated,
    roles([UserRole.Admin]),
    paginationValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await user.getUsers(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.get('/:id', idValidation, runValidation, 
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await user.getUser(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.put(
    '/:id',
    isAuthenticated,
    idValidation,
    updateUserValidation,
    runValidation,
    ownsAccount,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await user.updateUser(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.delete(
    '/:id',
    isAuthenticated,
    idValidation,
    runValidation,
    canAccess,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await user.deleteUser(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.get('/:userId/cv', userIdVallidation, runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await user.getCV(req,res);
      }catch(err){
        next(err);
      }
    });
  return router;
};
