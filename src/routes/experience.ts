import { ControllerContext, RouterFactory } from '../interfaces/general';
import express, { Request,NextFunction ,Response } from 'express';
import { UserRole } from '../models/user.model';
import { runValidation } from '../middleware/runValidation';
import roles from '../middleware/authorization';
import { idValidation, paginationValidation } from '../libs/validations/user';
import isAuthenticated from '../middleware/authentication';
import {
  createExperienceValidation,
  updateExperienceValidation,
} from '../libs/validations/experience';

export const makeExperienceRouter: RouterFactory = (context: ControllerContext) => {
  const router = express.Router();
  const experience = context.controllers.expeienceController;

  router.post(
    '/',
    isAuthenticated,
    roles([UserRole.Admin, UserRole.User]),
    createExperienceValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await experience.addExperice(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.get(
    '/',
    isAuthenticated,
    roles([UserRole.Admin]),
    paginationValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await experience.getExperiences(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.get('/:id', idValidation, runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await experience.getExperience(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.put(
    '/:id',
    isAuthenticated,
    idValidation,
    updateExperienceValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await experience.updateExperience(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.delete(
    '/:id',
    isAuthenticated,
    idValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await experience.deleteExperience(req,res);
      }catch(err){
        next(err);
      }
    }
  );

  return router;
};
