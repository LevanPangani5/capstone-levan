import { ControllerContext, RouterFactory } from '../interfaces/general';
import express, {Response, Request, NextFunction} from 'express';
import { UserRole } from '../models/user.model';
import { runValidation } from '../middleware/runValidation';
import roles from '../middleware/authorization';
import { idValidation, paginationValidation } from '../libs/validations/user';
import isAuthenticated from '../middleware/authentication';
import { feedValidation } from '../libs/validations/feedback';

export const makeFeedbackRouter: RouterFactory = (context: ControllerContext) => {
  const router = express.Router();
  const feedback = context.controllers.feedbackController;

  router.post(
    '/',
    isAuthenticated,
    roles([UserRole.Admin, UserRole.User]),
    feedValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await feedback.addFeedback(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.get(
    '/',
    isAuthenticated,
    roles([UserRole.Admin]),
    paginationValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await feedback.getFeedbacks(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.get('/:id', idValidation, runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await feedback.getFeedback(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.put(
    '/:id',
    isAuthenticated,
    idValidation,
    feedValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await feedback.updateFeedback(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.delete(
    '/:id',
    isAuthenticated,
    idValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await feedback.deleteFeedback(req,res);
      }catch(err){
        next(err);
      }
    }
  );

  return router;
};
