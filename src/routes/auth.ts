import { ControllerContext, RouterFactory } from '../interfaces/general';
import express from 'express';
import { NextFunction, Request, Response } from 'express';
import {
  loginValidation,
  registrationValidation,
} from '../libs/validations/auth';
import { runValidation } from '../middleware/runValidation';
import passport from 'passport';
export const makeAuthRouter: RouterFactory = (context: ControllerContext) => {
  const router = express.Router();
  const auth = context.controllers.authController;

  router.post(
    '/register',
    registrationValidation,
    runValidation,
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await auth.register(req,res);
      }catch(err){
        next(err);
      }
    }
  );
  router.post(
    '/login',
    loginValidation,
    runValidation,
    passport.authenticate('local', { session: false }),
    async (req: Request, res:Response, next: NextFunction)=>{
      try{
        await auth.login(req,res);
      }catch(err){
        next(err);
      }
    }
  );

  return router;
};
