/* eslint-disable @typescript-eslint/no-unused-vars */
import { Loader } from '../interfaces/general';
import express from 'express';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import requestId from 'express-request-id';
import { logger } from '../libs/logger';
import { logging } from '../middleware/logging';

export const loadMiddlewares: Loader = (app, context) => {
  app.use(
    session({
      name: 'session',
      secret: 'sdfax1444',
      resave: false,
      saveUninitialized: false,
    }),
  );

  app.use(express.urlencoded({ extended: true }));
  app.use(express.json());
  app.use(cookieParser());

  app.use(requestId());
  app.use(logging);

  process
    .on('uncaughtException', async (err) => {
      logger.error(err);
      process.exit(1);
    })
    .on('unhandledRejection', (reason) => {
      logger.error(reason);
    });
};
