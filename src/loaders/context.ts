import { ExperienceService } from '../services/experience.service';
import { Context, ControllerContext } from '../interfaces/general';
import { AuthService } from '../services/auth.service';
import { FeedbackService } from '../services/feedback.service';
import { ProjectService } from '../services/project.serice';
import { UserService } from '../services/user.service';
import { AuthController } from '../controllers/auth.controller';
import { ExperienceController } from '../controllers/experience.controller';
import { FeedbackController } from '../controllers/feedback.controller';
import { ProjectController } from '../controllers/project.controller';
import { UserController } from '../controllers/user.controller';

//using single tone pattern i did not liked the built in structure
// becuase calling this function multiple times would create new context objects every  time
let context: Context | null = null;
let controllerContext: ControllerContext | null = null;

export const loadContext = async (): Promise<Context> => {
  if (!context){
    context = {
      services: {
        authService: new AuthService(),
        expeienceService: new ExperienceService(),
        feedbackService: new FeedbackService(),
        projectService: new ProjectService(),
        userService: new UserService(),
      }
    };   
  }
  return context;
};


export const loadControllerContext = async(): Promise<ControllerContext> =>{
  const context= await loadContext();
  if(!controllerContext){
    controllerContext = {
      controllers:{
        authController: new AuthController(context.services.authService),
        expeienceController: new ExperienceController(context.services.expeienceService),
        feedbackController: new FeedbackController(context.services.feedbackService),
        projectController: new ProjectController(context.services.projectService),
        userController: new UserController(context.services.userService)
      }
    };
  }
  return controllerContext;
};
