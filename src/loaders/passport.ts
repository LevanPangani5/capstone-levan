/* eslint-disable @typescript-eslint/no-unused-vars */
import { Context, Loader } from '../interfaces/general';
import passport from '../libs/passport';
export const loadPassport: Loader = (app, context) => {
  app.use(passport.initialize);
  app.use(passport.session);
  app.use(passport.setUser);
};
