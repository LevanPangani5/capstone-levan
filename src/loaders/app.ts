import { loadMiddlewares } from './middlewares';
import { loadRoutes } from './routes';
import express from 'express';
import { loadControllerContext , loadContext} from './context';
import { loadModels } from './models';
import { loadSequelize } from './sequelize';
import { config } from '../config';
import { loadPassport } from './passport';
import { loadRedis } from './redis';

export const loadApp = async (): Promise<express.Express> => {
  const app = express();

  const sequelize = loadSequelize(config);
  loadModels(sequelize);
  await loadRedis(config);

  const context = await loadContext();
  const controlerContext = await loadControllerContext();
 
  loadMiddlewares(app, context);
  loadPassport(app, context);
  loadRoutes(app, controlerContext);

  return app;
};
