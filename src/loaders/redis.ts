import { Cache } from '../libs/cache';
import { Config } from '../config';
import { logger } from '../libs/logger';

let cache: Cache | null = null;
export const loadRedis = async (config: Config): Promise<void> => {
  if (!cache) {
    cache = new Cache(config.redis);
    try {
      await cache.connect();
    } catch (err) {
      await cache.disconnect();
      logger.error(err);
      cache = null;
    }
  }
};

export { cache };
