import express from 'express';
import { ControllerContext } from '../interfaces/general';
import { makeAuthRouter } from '../routes/auth';
import { makeExperienceRouter } from '../routes/experience';
import { makeFeedbackRouter } from '../routes/feedback';
import { makeProjectRouter } from '../routes/project';
import { makeUserRouter } from '../routes/user';
import { profileUpload, projectUpload } from '../libs/MultiPart';
import {
  globalErrorHandler,
  routerErrorHAndler,
} from '../middleware/handleErrors';
import multer from 'multer';

const form = multer();
export const loadRoutes = async (
  app: express.Router,
  context: ControllerContext,
): Promise<void> => {

  app.use('/api/auth', profileUpload.single('image'), makeAuthRouter(context));
  app.use('/api/users', profileUpload.single('image'), makeUserRouter(context));
  app.use('/api/experience', form.none(), makeExperienceRouter(context));
  app.use('/api/feedback', form.none(), makeFeedbackRouter(context));
  app.use('/api/projects',projectUpload.single('image'),makeProjectRouter(context));

  app.all('*', routerErrorHAndler);
  app.use(globalErrorHandler);
};
